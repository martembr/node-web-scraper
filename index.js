const $ = require('cheerio');
const rp = require('request-promise');
const fs = require('fs');

const url = "https://en.wikipedia.org/wiki/Poodle"; 


// get html file from website
rp(url)
    .then(function(html) {
        const infoArr = [];
        let headers = $('h2 > span', html);
        for (let i=0; i < headers.length; i++) {
            infoArr.push(headers[i].children[0].data);
        }
        //make json object
        infojson = {
            data: infoArr
        };
        // make into string
        infotext = JSON.stringify(infojson, null, 2);
        // write to file
        writeJsonFile('info.json', infotext);
    })
    .catch(function(err){
        console.log("something went wrong");
    });


// takes in filename and data and writes to file
function writeJsonFile(filename, data) {
    fs.writeFile(filename, data, (err) => {
        if (err) throw err;
        console.log('The file has been saved!');
      });
}